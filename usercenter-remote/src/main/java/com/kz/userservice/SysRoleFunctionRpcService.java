package com.kz.userservice;

import com.kz.remote.vo.SysRoleFunctionVO;

import java.util.List;

/**
 * 角色-功能
 *
 * @author kz
 * @date 2017-12-19
 */
public interface SysRoleFunctionRpcService {


    /**
     * 查询角色具有的功能
     *
     * @param roleId
     * @return
     */
    List<SysRoleFunctionVO> queryRoleFunctionList(String roleId);
}
