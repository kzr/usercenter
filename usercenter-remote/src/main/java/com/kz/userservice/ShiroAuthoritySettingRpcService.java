package com.kz.userservice;

import com.kz.entity.sys.ShiroAuthoritySetting;

import java.util.List;

/**
 * shiro拦截url配置
 *
 * @author kz
 * @date 2017-12-19
 */
public interface ShiroAuthoritySettingRpcService {


    /**
     * 查询shiro拦截url配置
     *
     * @return
     */
    List<ShiroAuthoritySetting> queryShiroUrlList();
}
