package com.kz.userservice;

import com.kz.entity.sys.SysUser;

/**
 *
 * @author 0
 * @date 2017-12-19
 */
public interface SysUserRpcService {

    /**
     * 注册用户
     * @param sysUser
     */
    void save(SysUser sysUser);

    void update(SysUser sysUser);

    Integer querySysUserByEmail(String email);

    SysUser selectOne(String username,String password);
}
