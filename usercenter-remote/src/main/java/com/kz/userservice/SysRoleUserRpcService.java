package com.kz.userservice;

/**
 * 角色-用户
 *
 * @author kz
 * @date 2017-12-19
 */
public interface SysRoleUserRpcService {


    /**
     * 根据用户ID,查询角色ID
     *
     * @param userId
     * @return
     */
    String getRoleIdByUserId(String userId);
}
