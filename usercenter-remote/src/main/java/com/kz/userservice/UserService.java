package com.kz.userservice;


import com.kz.vo.OperateResult;
import com.kz.vo.UserBindVo;
import com.kz.vo.UserVo;

/**
 *
 * @author rain
 * @date 2015/11/20
 */
public interface UserService {
	/**
	 * 修改密码
	 *
	 * @param userId      用户ID,智慧会务用户ID与shopId已经合并
	 * @param oldPassword 老密码
	 * @param newPassword 新密码
	 * @return
	 */
	public OperateResult changePassword(String userId, String oldPassword, String newPassword);

	/**
	 * 检验用户名密码是否正确
	 *
	 * @param loginId 用户登录ID
	 * @param password 密码
	 * @return
	 */
	public boolean checkPassword(String loginId, String password);

	/**
	 * 新增用户
	 *
	 * @param vo
	 * @return
	 */
	public OperateResult insertUser(UserVo vo);

	/**
	 * 修改用户
	 *
	 * @param vo
	 * @return
	 */
	public OperateResult updateUser(UserVo vo);

	/**
	 * 根据ID查询用户
	 *
	 * @param userId
	 * @return
	 */
	public UserVo findByUserId(String userId);

	/**
	 * 根据登录ID查询用户
	 * @param loginId
	 * @return
	 */
	public UserVo findByLoginId(String loginId);

	/**
	 * 重置密码
	 * @param userId
	 * @param newPassword
	 */
	public OperateResult resetPassword(String userId, String newPassword);

	/**
	 * 检查登录ID是否存在
	 * @param loginId
	 * @return
	 */
	public boolean checkLoginIdIsExist(String loginId);

	/**
	 * 根据openId和登录类型查询绑定用户
	 * @param openId
	 * @param loginType
	 * @data 2016/09/07
	 * @return
	 */
	public UserBindVo findByOpenIdAndLoginType(String openId, String loginType);

	/**
	 * 绑定用户
	 * 如果用户已经存在则直接关联，不存在则注册
	 * @param vo
	 * @return
	 */
	public OperateResult bindUser(UserVo vo);

}
