/**
 * Copyright (C) 2014 serv (liuyuhua69@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kz.em;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 性别枚举
 * @author serv
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Gender {

	/**
	 * 女
	 */
	FEMALE ("女"),
	
	/**
	 * 男
	 */
	MALE   ("男");
 
	private String value ;

	private Gender(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
