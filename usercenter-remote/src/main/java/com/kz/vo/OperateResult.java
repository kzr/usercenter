package com.kz.vo;

import java.io.Serializable;

/**
 * Created by Carmel on 2014/11/13.
 */
public class OperateResult implements Serializable{
	public OperateResult() {

	}

	public OperateResult(String message) {
		this(false, message);
	}

	public OperateResult(boolean success, String message) {
		this.success = success;
		this.messsage = message;
	}

	private boolean success = true;
	private String messsage = "操作成功";

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMesssage() {
		return messsage;
	}

	public void setMesssage(String messsage) {
		this.messsage = messsage;
	}
}
