package com.kz.vo;

/**
 *
 * @author serv
 * @date 2014/10/14
 */
public abstract class BaseInfoVo extends BaseVo{
    /**
     * 公司名称
     */
    protected String name;

    /**
     * 简称
     */
    protected String shortName;

    /**
     * 状态
     */
    protected boolean enable = Boolean.TRUE ;


    /**
     * 拼音
     */
    private String spell;

    /**
     * 简拼
     */
    private String shortSpell;

    /**
     * 备注
     */
    protected String remark;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getSpell() {
        return spell;
    }

    public void setSpell(String spell) {
        this.spell = spell;
    }

    public String getShortSpell() {
        return shortSpell;
    }

    public void setShortSpell(String shortSpell) {
        this.shortSpell = shortSpell;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
