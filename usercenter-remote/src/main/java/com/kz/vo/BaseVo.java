package com.kz.vo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author serv
 * @date 2014/10/14
 */
public abstract class BaseVo implements Serializable{

    //id
    protected String id;
    //创建时间
    protected Date createDate =  null ;

    //更新时间
    protected Date auditDate;

    //删除状态
    protected boolean deleteStatus = Boolean.FALSE;


    //默认排序值
    protected Long sort  = 0L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public boolean isDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(boolean deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }
}
