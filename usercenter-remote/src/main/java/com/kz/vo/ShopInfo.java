package com.kz.vo;

/**
 * Created by Carmel on 2014/12/15.
 */
public class ShopInfo extends BaseInfoVo{
	/**
	 * 是否是渠道商
	 */
	private Boolean isChannel;

	/**
	 * 渠道商的shopId
	 */
	private String shopChannelId;

	/**
	 * 代理商ID
	 */
	private String supplierId;

	/**
	 * 华阳佣金比例
	 */
	private Double profitScale;

	/**
	 * 经度
	 */
	private Double latitude;

	/**
	 * 纬度
	 */
	private Double longitude;

	/**
	 * 编号、商家代码
	 */
	private String code;
	/**
	 * 类别1
	 */
	private String category1;

	/**
	 * 类别2
	 */
	private String category2;

	/**
	 * 省
	 */
	private String province;

	/**
	 * 市
	 */
	private String city;

	/**
	 * 区县
	 */
	private String county;

	/**
	 * 街道乡镇
	 */
	private String town;

	/**
	 * 电话
	 */
	private String phone;
	/**
	 * 手机
	 */
	private String mobile;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 地址
	 */
	private String address;

	/**
	 * 最全地址
	 */
	private String fullAddress;

	/**
	 * 传真
	 */
	private String fax;
	/**
	 * 联系人
	 */
	private String contact;


	/**
	 * 创建商家时的门店开通单价
	 */
	private Integer initStoreFee;

	/**
	 * 创建商家的时候开通的门店数量
	 */
	private Integer initStoreCount;

	/**
	 * 渠道经理ID
	 */
	private String channelManagerId;

	/**
	 * 来源(0 渠道;1 直销;2 测试)
	 */
	private String sourceFlag;

	private String shopLogo ;


	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCategory1() {
		return category1;
	}

	public void setCategory1(String category1) {
		this.category1 = category1;
	}

	public String getCategory2() {
		return category2;
	}

	public void setCategory2(String category2) {
		this.category2 = category2;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}



	public Integer getInitStoreFee() {
		return initStoreFee;
	}

	public void setInitStoreFee(Integer initStoreFee) {
		this.initStoreFee = initStoreFee;
	}

	public Integer getInitStoreCount() {
		return initStoreCount;
	}

	public void setInitStoreCount(Integer initStoreCount) {
		this.initStoreCount = initStoreCount;
	}

	public Double getProfitScale() {
		return profitScale;
	}

	public void setProfitScale(Double profitScale) {
		this.profitScale = profitScale;
	}

	public String getShopChannelId() {
		return shopChannelId;
	}

	public void setShopChannelId(String shopChannelId) {
		this.shopChannelId = shopChannelId;
	}

	public Boolean getIsChannel() {
		return isChannel;
	}

	public void setIsChannel(Boolean isChannel) {
		this.isChannel = isChannel;
	}

	public String getChannelManagerId() {
		return channelManagerId;
	}

	public void setChannelManagerId(String channelManagerId) {
		this.channelManagerId = channelManagerId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSourceFlag() {
		return sourceFlag;
	}

	public void setSourceFlag(String sourceFlag) {
		this.sourceFlag = sourceFlag;
	}

	public String getShopLogo() {
		return shopLogo;
	}

	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}
}
