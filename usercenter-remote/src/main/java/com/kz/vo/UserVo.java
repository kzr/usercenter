package com.kz.vo;

import com.kz.em.Gender;

import java.util.Date;

/**
 * 用户信息
 */
public class UserVo extends BaseInfoVo{

    /**
     * 经度
     */
    private Double latitude;

    /**
     * 纬度
     */
    private Double longitude;

    /**
     * 类别1
     */
    private String category1;

    /**
     * 类别2
     */
    private String category2;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区县
     */
    private String county;

    /**
     * 街道乡镇
     */
    private String town;
    /**
     * 最全地址
     */
    private String fullAddress;

    /**
     * 手机
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 用户来源
     */
    private String sourceFlag;

    private String shopLogo ;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 生日
     */
    private Date birthDay;
    /**
     * 是否为农历
     */
    private boolean isLunarCalendar = false;

    /**
     * 工作固话
     */
    private String workTel;

    /**
     * 家庭固话
     */
    private String familyTel;

    /**
     * 传真
     */
    private String fax;

    /**
     * 用户性别
     */
    private Gender gender = Gender.MALE;

	/**
	 * 是否被激活
	 */
	private Boolean active;

    //用户头像
    private String avatar;

    private Boolean isDelete ;

    /**登录ID*/
    private String loginId;

    /**授权ID*/
    private String openId;

    /**登录类型*/
    private String loginType;

    /**
     * 编号、商家代码
     */
    private String code;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getCategory1() {
        return category1;
    }

    public void setCategory1(String category1) {
        this.category1 = category1;
    }

    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        this.category2 = category2;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getSourceFlag() {
        return sourceFlag;
    }

    public void setSourceFlag(String sourceFlag) {
        this.sourceFlag = sourceFlag;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public boolean isLunarCalendar() {
        return isLunarCalendar;
    }

    public void setLunarCalendar(boolean lunarCalendar) {
        isLunarCalendar = lunarCalendar;
    }

    public String getWorkTel() {
        return workTel;
    }

    public void setWorkTel(String workTel) {
        this.workTel = workTel;
    }

    public String getFamilyTel() {
        return familyTel;
    }

    public void setFamilyTel(String familyTel) {
        this.familyTel = familyTel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "UserVo{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", category1='" + category1 + '\'' +
                ", category2='" + category2 + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", town='" + town + '\'' +
                ", fullAddress='" + fullAddress + '\'' +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", contact='" + contact + '\'' +
                ", sourceFlag='" + sourceFlag + '\'' +
                ", shopLogo='" + shopLogo + '\'' +
                ", password='" + password + '\'' +
                ", birthDay=" + birthDay +
                ", isLunarCalendar=" + isLunarCalendar +
                ", workTel='" + workTel + '\'' +
                ", familyTel='" + familyTel + '\'' +
                ", fax='" + fax + '\'' +
                ", gender=" + gender +
                ", active=" + active +
                ", avatar='" + avatar + '\'' +
                ", isDelete=" + isDelete +
                ", loginId='" + loginId + '\'' +
                '}';
    }
}
