package com.kz.vo.page;

import java.io.Serializable;

/**
 *
 * @author Carmel
 * @date 2014/9/25
 */
public class PageableVo implements Serializable {
	/**
	 * 第几页 从0 开始
	 */
	private int page;
	/**
	 * 每页多少条
	 */
	private int size;
	/**
	 * 排序
	 */
	private SortVo sort;

	public PageableVo(int page, int size) {
		this.page = page;
		this.size = size;
	}

	public PageableVo(int page, int size, SortVo sort) {
		this.page = page;
		this.size = size;
		this.sort = sort;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public SortVo getSort() {
		return sort;
	}

	public void setSort(SortVo sort) {
		this.sort = sort;
	}

}
