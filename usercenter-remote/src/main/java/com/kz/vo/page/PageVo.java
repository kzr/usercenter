package com.kz.vo.page;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Carmel on 2014/9/26.
 */
public class PageVo<T> implements Serializable {
	/**
	 * 第几页
	 */
	private int page;
	/**
	 * 每页多少条
	 */
	private int size;
	/**
	 * 总数
	 */
	private long total;
	/**
	 * 结果集
	 */
	private List<T> content;
	/**
	 * 排序
	 */
	private SortVo sort;

	public int getTotalPages() {
		return getSize() == 0 ? 1 : (int) Math.ceil((double) total / (double) getSize());
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public SortVo getSort() {
		return sort;
	}

	public void setSort(SortVo sort) {
		this.sort = sort;
	}
}
