package com.kz.vo.page;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author serv
 * @date 2014/11/11
 */
public class SortVo implements Serializable {

    private List<Order> orders;

    public SortVo() {
    }

    public SortVo(Order... orders) {
        this(Arrays.asList(orders));
    }

    public SortVo(List<Order> orders) {
        this.orders = orders;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public static class Order implements Serializable{

        private Direction direction;
        private String property;

        public Order() {
        }
        public Order(Direction direction, String property) {
            this.direction = direction;
            this.property = property;
        }

        public Direction getDirection() {
            return direction;
        }

        public void setDirection(Direction direction) {
            this.direction = direction;
        }

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public static enum Direction {
            ASC, DESC;
        }
    }
}
