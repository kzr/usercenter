package com.kz.vo;

/**
 * Created by serv on 2014/10/24.
 */
public class DeviceInfoVo extends BaseInfoVo{

    private String shopId;

    private String shopName;

    private String code;

    //终端来源，1表示 新平台添加，0，表示老系统导入
    private String flag ;
    //密码
    private String passCode;

    //老系统终端号
    private String oldPosNo ;

    /**
     * 设备类型
     */
    private String deviceType ;
    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getOldPosNo() {
        return oldPosNo;
    }

    public void setOldPosNo(String oldPosNo) {
        this.oldPosNo = oldPosNo;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
