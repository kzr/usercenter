package com.kz.vo;

import java.io.Serializable;

/**
 * Created by rain.wen on 2016/9/7.
 */
public class UserBindVo implements Serializable {

    private Long userBindId; //绑定用户id

    private String userId; //引用用户id

    private String nickName; //昵称

    private String headImgUrl; //头像

    private String loginType; //登录类型

    private String openId; //开放平台ID

    public Long getUserBindId() {
        return userBindId;
    }

    public void setUserBindId(Long userBindId) {
        this.userBindId = userBindId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
