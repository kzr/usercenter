package com.kz.usercenter.rpc.user;

import com.kz.remote.vo.SysRoleFunctionVO;
import com.kz.service.sys.SysRoleFunctionService;
import com.kz.userservice.SysRoleFunctionRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kz
 * @date 2017-12-19
 */
@Service
public class SysRoleFunctionRpcServiceImpl implements SysRoleFunctionRpcService {


    @Autowired
    private SysRoleFunctionService sysRoleFunctionService;

    /**
     * 查询角色具有的功能
     *
     * @param roleId
     * @return
     */
    @Override
    public List<SysRoleFunctionVO> queryRoleFunctionList(String roleId) {
        return sysRoleFunctionService.queryRoleFunctionList(roleId);
    }
}
