package com.kz.usercenter.rpc.user;

import com.kz.entity.sys.SysUser;
import com.kz.service.sys.SysUserService;
import com.kz.userservice.SysUserRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kz
 * @date 2017-12-19
 */
@Service
public class SysUserRpcServiceImpl implements SysUserRpcService {

    @Autowired
    private SysUserService sysUserService;

    @Override
    public void save(SysUser sysUser) {
        sysUserService.save(sysUser);
    }

    @Override
    public void update(SysUser sysUser) {
        sysUserService.update(sysUser);
    }

    @Override
    public Integer querySysUserByEmail(String email) {
        return sysUserService.querySysUserByEmail(email);
    }

    @Override
    public SysUser selectOne(String username, String password) {
        return sysUserService.selectOne(username, password);
    }
}
