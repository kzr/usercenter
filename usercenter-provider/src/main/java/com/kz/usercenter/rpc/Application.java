package com.kz.usercenter.rpc;


import com.kz.cache.JdkRedisTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.CountDownLatch;

/**
 * @author kz
 */
@Configuration
@EnableAutoConfiguration
@EnableScheduling
@EnableAsync
@MapperScan("com.kz.persistence")
@ComponentScan(basePackages = "com.kz")
@PropertySource({"classpath:application.yml", "classpath:config.properties"})
@ImportResource({"classpath:dubbo.xml"})
public class Application {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Application.class, args);
        new CountDownLatch(1).await();
    }

    @Bean
    public JdkRedisTemplate jdkRedisTemplate(RedisConnectionFactory connectionFactory){
        JdkRedisTemplate jdkRedisTemplate = new JdkRedisTemplate();
        jdkRedisTemplate.setConnectionFactory(connectionFactory);
        return jdkRedisTemplate ;
    }

}
