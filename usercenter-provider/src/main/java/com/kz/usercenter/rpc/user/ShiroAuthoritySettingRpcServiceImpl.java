package com.kz.usercenter.rpc.user;

import com.kz.entity.sys.ShiroAuthoritySetting;
import com.kz.service.sys.ShiroAuthoritySettingService;
import com.kz.userservice.ShiroAuthoritySettingRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 查询shiro拦截url配置
 *
 * @author kz
 * @date 2017-12-19
 */
@Service
public class ShiroAuthoritySettingRpcServiceImpl implements ShiroAuthoritySettingRpcService {


    @Autowired
    private ShiroAuthoritySettingService shiroAuthoritySettingService;

    /**
     * 查询shiro拦截url配置
     *
     * @return
     */
    @Override
    public List<ShiroAuthoritySetting> queryShiroUrlList() {
        return shiroAuthoritySettingService.queryShiroUrlList();
    }
}
