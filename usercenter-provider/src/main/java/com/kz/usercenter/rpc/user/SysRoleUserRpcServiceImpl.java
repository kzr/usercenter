package com.kz.usercenter.rpc.user;

import com.kz.service.sys.SysRoleUserService;
import com.kz.userservice.SysRoleUserRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kz
 * @date 2017-12-19
 */
@Service
public class SysRoleUserRpcServiceImpl implements SysRoleUserRpcService {


    @Autowired
    private SysRoleUserService sysRoleUserService;

    /**
     * 根据用户ID,查询角色ID
     *
     * @param userId
     * @return
     */
    @Override
    public String getRoleIdByUserId(String userId) {
        return sysRoleUserService.getRoleIdByUserId(userId);
    }
}
